﻿using System;
namespace ThomasUndTobias.models
{
    public class Handy
    {
        public string Name { get; set; }
        public string ID { get; set; }

        public Handy() : this("", "") {}
        public Handy(string name, string id) {
            this.Name = name;
            this.ID = id; 
        }

        public override string ToString()
        {
            return string.Format("[Handy: Name={0}, ID={1}]", Name, ID);
        }
    }
}
