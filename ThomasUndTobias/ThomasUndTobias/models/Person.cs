﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThomasUndTobias
{
    enum Gender
    {
        Male,
        Female,
        Unknown
    }

    class Person
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        private int age;

        public int Age
        {
            get
            {
                return this.age;
            }
            set
            {
                if(value >= 0)
                {
                    this.age = value;
                }
                else
                {
                    throw new ArgumentException("Age cannot have a value less than 0.");
                }
            }
        }

        public Gender Gender { get; set; }

        public Person(string firstname, string lastname, int age, Gender gender)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Age = age;
            this.Gender = gender;
        }

        public Person() : this("", "", 0, Gender.Unknown)
        {

        }

        public override string ToString()
        {
            return "Firstname: " + this.Firstname + " Lastname: " + this.Lastname + " Age: "
                + this.Age + " Gender: " + this.Gender + Environment.NewLine;
        }
    }
}
